<?php

namespace Pantagruel74\Yii2FileActiveRecord;

use yii\base\Model;

class FileDbTool extends Model
{
    public array $classes;
    public string $fileDbComponentName = 'filedb';
    public string $initMsg = '';

    /**
     * @return void
     */
    public function initActiveRecordFiles(): void
    {
        $dbActiveRecordClasses = array_filter(
            $this->classes,
            fn(string $c) => (is_subclass_of($c, FileActiveRecord::class))
        );
        /* @var array<FileActiveRecord|class-string> $dbActiveRecordClasses */
        $dbFileNames = array_map(fn(string $c) => ($c::fileName()), $dbActiveRecordClasses);
        $ds = DIRECTORY_SEPARATOR;
        $fdbcn = $this->fileDbComponentName;
        foreach ($dbFileNames as $dbf)
        {
            $filedbPath = \Yii::getAlias(\Yii::$app->$fdbcn->path);
            if(!is_dir($filedbPath)) {
                mkdir($filedbPath);
            }
            if (!empty($this->initMsg)) {
                echo $this->initMsg . ' ' . $filedbPath . $ds . $dbf . ".php ...\n";
            }
            file_put_contents($filedbPath . $ds . $dbf . '.php', "<?php\nreturn [];\n");
        }
    }
}