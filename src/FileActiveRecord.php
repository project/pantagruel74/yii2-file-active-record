<?php

namespace Pantagruel74\Yii2FileActiveRecord;

use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;
use yii\db\ActiveRecordInterface;
use yii2tech\filedb\ActiveRecord;

class FileActiveRecord extends ActiveRecord
{
    const NEW_RECORD_PK_STRATEGY_AIID = 'aiid';
    const NEW_RECORD_PK_STRATEGY_UUID = 'uuid';

    /**
     * @return string
     */
    public static function newRecordPKStrategy(): string
    {
        return static::NEW_RECORD_PK_STRATEGY_AIID;
    }

    /**
     * @param string $strategy
     * @return void
     */
    public function createNewRecordPK(string $strategy): void
    {
        if($strategy === static::NEW_RECORD_PK_STRATEGY_AIID) {
            Assert::count(static::primaryKey(), 1,
                "New record primary key creation strategy 'AIID' means primary key should consist of 1 attribute");
            $pk = current(static::primaryKey());
            try {
                $this->$pk = $this->getLastPK() + 1;
            } catch (\Error $error) {
                $this->$pk = 1;
            }
        } elseif($strategy === static::NEW_RECORD_PK_STRATEGY_UUID) {
            Assert::count(static::primaryKey(), 1,
                "New record primary key creation strategy 'UUID' means primary key should consist of 1 attribute");
            $pk = current(static::primaryKey());
            $this->$pk = static::generateUuid();
        } else {
            throw new \Error("Unknown new record Primary key creating strategy: " . $strategy);
        }
    }

    /**
     * @return string
     */
    protected static function generateUuid(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @param bool $runValidation
     * @param array|null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if($this->checkPK() !== true) {
            $this->createNewRecordPK(static::newRecordPKStrategy());
        }
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @return bool
     */
    protected function checkPK(): bool
    {
        if (count(static::primaryKey()) !== 1) {
            return false;
        }
        $pk = current(static::primaryKey());
        return isset($this->$pk);
    }

    /**
     * @return int|string
     */
    public function getLastPK()
    {
        $ids = array_map(fn(ActiveRecordInterface $ar) => ($ar->id), static::find()->all());
        return empty($ids) ? 0 : max($ids);
    }
}